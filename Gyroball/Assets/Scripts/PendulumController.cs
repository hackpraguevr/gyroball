﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PendulumController : MonoBehaviour {

	public bool reverse;

	AnimationCurve animationCurve;
	Stopwatch stopwatch;
	Vector3 lastRotation = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {
		animationCurve = AnimationCurve.EaseInOut (0, -45, 2000, 45);
		ResetStopwatch ();
	}
	
	// Update is called once per frame
	void Update () {		
		if (stopwatch.ElapsedMilliseconds > 2000) {
			ResetStopwatch ();
			reverse = !reverse;
		}

		float value = animationCurve.Evaluate (stopwatch.ElapsedMilliseconds);
		if (reverse)
			value = 1 - value;

		Vector3 rotation = new Vector3 (0, 0, value);
		transform.Rotate (rotation - lastRotation);
		lastRotation = rotation;

	}

	void ResetStopwatch() {
		stopwatch = new Stopwatch ();
		stopwatch.Start ();
	}
}

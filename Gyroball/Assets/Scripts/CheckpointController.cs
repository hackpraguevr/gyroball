﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour {

    public Material[] material;
    Renderer rend;
	AudioSource audioSource;

    void Start () {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];

		audioSource = GetComponent<AudioSource> ();
	}

    private void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.CompareTag ("Player") && rend.sharedMaterial != material[1]) {
			rend.sharedMaterial = material [1];
			audioSource.Play ();
		}
    }
}

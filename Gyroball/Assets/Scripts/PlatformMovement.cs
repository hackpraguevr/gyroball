﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PlatformMovement : MonoBehaviour {

    Vector3 direction;
	bool reverse = false;
    Stopwatch stopwatch;
    bool firstTime = true;
    long randomWait;
    static System.Random random = new System.Random();
	AnimationCurve animationCurve;
	Vector3 lastTranslate = new Vector3(0,0,0);

    void Start()
    {
        direction = new Vector3(4, 0, 0);
        ResetStopwatch();
        randomWait = random.Next(100, 2000);
		animationCurve = AnimationCurve.EaseInOut (0, 0f, 2000, 1f);
    }

    void Update()
    {
        if ( firstTime )
        {
            if (stopwatch.ElapsedMilliseconds < randomWait)
                return;
            firstTime = false;
            ResetStopwatch();
            return;
        }
			        
		if (stopwatch.ElapsedMilliseconds > 2000)
		{
			ResetStopwatch();
			reverse = !reverse;
		}

		float value = animationCurve.Evaluate (stopwatch.ElapsedMilliseconds);
		if (reverse)
			value = 1 - value;
		Vector3 translate = direction * value;
		transform.Translate(translate - lastTranslate);
		lastTranslate = translate;
    }

    void ResetStopwatch()
    {
        stopwatch = new Stopwatch();
        stopwatch.Start();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class WaterfallGenerator : MonoBehaviour {

    public Transform prefab;
    private static System.Random random = new System.Random();
    private Stopwatch stopwatch;

	// Use this for initialization
	void Start () {
        ResetStopwatch();
    }
	
	// Update is called once per frame
	void Update () {
        if (stopwatch.ElapsedMilliseconds < 1000) return;
        ResetStopwatch();
        if (random.Next(3) != 0) return;
        GenerateParticle();
	}

    private void ResetStopwatch()
    {
        stopwatch = new Stopwatch();
        stopwatch.Start();
    }

    void GenerateParticle()
    {
        var x = -1 + random.Next(20) / 10.0f;
        var z = -1 + random.Next(20) / 10.0f;
        var a = random.Next(360);
        var b = random.Next(360);
        var c = random.Next(360);
        var particle = Instantiate(prefab, transform.position - new Vector3(x, 1.5f, z), Quaternion.identity);
        particle.Rotate(new Vector3(a, b, c));
    }
}

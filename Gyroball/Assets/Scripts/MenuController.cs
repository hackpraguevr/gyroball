﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

	public string scene;

	// Use this for initialization
	void Start () {		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerEnter(PointerEventData data)
	{
		Debug.Log("OnPointerEnter called.");
		GetComponent<Renderer>().material.color = new Color32(0xD1, 0xC4, 0xE9, 0xFF);	
	}

	public void OnPointerExit(PointerEventData data)
	{
		Debug.Log("OnPointerExit called.");
		GetComponent<Renderer>().material.color = Color.white;	
	}

	public void OnPointerClick(PointerEventData data)
	{
		GetComponent<Renderer>().material.color = new Color32(0xB3, 0x9D, 0xDB, 0xFF);
		SceneManager.LoadScene (scene);
	}
}

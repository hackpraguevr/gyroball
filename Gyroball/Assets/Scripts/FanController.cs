﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float speed = 360;
		float x = transform.rotation.x + Time.deltaTime * speed;
		transform.Rotate (x, 0, 0);
	}
}

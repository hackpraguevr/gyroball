﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class PlayerController : MonoBehaviour {

	public int speed;
	public GameObject text;
	public GameObject finishContainer;

    public AudioClip winClip;
    public AudioClip gameOverClip;
    public AudioClip pointClip;

	private Rigidbody rb;
	private int count;
	private TextMesh textMesh;
    bool canJump = false;
    private int score = 0;
    public Text scoreText;

    private Vector3 spawnLocation;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		count = 0;
		textMesh = text.GetComponent<TextMesh> ();
        spawnLocation = transform.position;
        UpdateScore();
	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate () {
		if (GvrController.AppButtonUp) {
			SceneManager.LoadScene ("Menu");
		}

		if (finishContainer != null && finishContainer.activeInHierarchy)
			return;

		Vector3 accel = GvrController.Accel;
		//Debug.Log ("accel:" + accel);

		float a = 1f;
		float moveHorizontal = -a*accel.x;
		float moveVertical = -a*accel.z;
		Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);
		rb.AddForce (movement);

		/*
		if (GvrController.IsTouching) {
			Vector2 touchPos = GvrController.TouchPos;
			float moveHorizontal = 2 * touchPos.x - 1f;
			float moveVertical = -2 * touchPos.y + 1f;
			Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
			rb.AddForce (movement * speed);
		}
		*/

		// Keyboard control
		moveHorizontal = Input.GetAxis ("Horizontal");
		moveVertical = Input.GetAxis ("Vertical");
		movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * speed);

		if (GvrController.ClickButtonDown || Input.GetKeyDown("space"))
            Jump();
    }

	void LateUpdate() {
		float currentHeight = transform.position.y;
		if (currentHeight < -5) {
            Respawn();
            //SceneManager.LoadScene ("GameScene1");
		}
	}

	void OnTriggerEnter (Collider other) {
        /*
		if (other.gameObject.CompareTag ("Pickup")) {
			other.gameObject.SetActive (false);
			count++;
		}
		*/

		Debug.Log ("OnTriggerEnter:" + other);

		if (other.gameObject.CompareTag ("Finish")) {
			Finish ();
		} else if (other.gameObject.CompareTag ("Floor")) {
			Respawn ();
        }
        else if (other.gameObject.CompareTag("Checkpoint"))
        {
            spawnLocation = other.gameObject.transform.position;
        }
        else if (other.gameObject.CompareTag("Collectable"))
        {
            score++;
            other.gameObject.SetActive(false);
            UpdateScore();
            GetComponent<AudioSource>().PlayOneShot(pointClip);
        }
    }

	void OnTriggerStay (Collider other) {
		/*
		if (other.gameObject.CompareTag("Wind")) {
			float windForce = -15;
			Vector3 movement = new Vector3(windForce, 0, 0);
			rb.AddForce(movement);
		}
		*/
	}

    private void Respawn()
    {
        transform.position = spawnLocation;
        rb.velocity = Vector3.zero;

		rb.angularVelocity = Vector3.zero;
        GetComponent<AudioSource>().PlayOneShot(gameOverClip);
    }

    private void OnCollisionEnter(Collision collision)
    {
        canJump = true;
    }

    private void Jump()
    {
        if (!canJump) return;
        rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        canJump = false;
    }

    private void UpdateScore()
    {		
    	 scoreText.text = "Score: " + score.ToString();
    }

	private void Finish()
	{
		if (finishContainer != null)
			finishContainer.SetActive (true);
		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;

        GetComponent<AudioSource>().PlayOneShot(winClip);
    }
}

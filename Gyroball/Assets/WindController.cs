﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindController : MonoBehaviour {

	public int force;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other) {				
		Vector3 movement = new Vector3(force, 0, 0);
		other.GetComponent<Rigidbody>().AddForce(movement);
	}
}
